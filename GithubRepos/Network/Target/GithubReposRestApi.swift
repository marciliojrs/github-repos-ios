//
//  GithubReposRestApi.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Moya

enum GithubReposRestApi {
    case pullRequests(owner: String, repo: String)
}

extension GithubReposRestApi: TargetType {

    var baseURL: URL {
        return URL(string: "https://api.github.com")!
    }

    var path: String {
        switch self {
        case .pullRequests(let owner, let repo): return "/repos/\(owner)/\(repo)/pulls"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var parameters: [String: Any]? {
        return nil
    }

    var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    var task: Task {
        return .request
    }

    var url: String {
        return "\(baseURL)\(path)".urlEscaped
    }

}

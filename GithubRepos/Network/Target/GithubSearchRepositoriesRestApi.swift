//
//  GithubRestApi.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 06/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Moya

enum GithubSearchRepositoriesRestApi {
    case repositories(Int)
}

extension GithubSearchRepositoriesRestApi: TargetType {

    var baseURL: URL {
        return URL(string: "https://api.github.com")!
    }

    var path: String {
        switch self {
        case .repositories(_): return "/search/repositories"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var parameters: [String: Any]? {
        switch self {
        case .repositories(let page): return ["q": "language:Java", "sort":"stars", "page": page]
        }
    }

    var sampleData: Data {
        switch self {
        case .repositories: return "{\"total_count\":2587873,\"incomplete_results\":false,\"items\":[{\"id\":507775,\"name\":\"elasticsearch\",\"full_name\":\"elastic/elasticsearch\",\"owner\":{\"login\":\"elastic\",\"id\":6764390,\"avatar_url\":\"https://avatars.githubusercontent.com/u/6764390?v=3\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/elastic\",\"html_url\":\"https://github.com/elastic\",\"followers_url\":\"https://api.github.com/users/elastic/followers\",\"following_url\":\"https://api.github.com/users/elastic/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/elastic/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/elastic/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/elastic/subscriptions\",\"organizations_url\":\"https://api.github.com/users/elastic/orgs\",\"repos_url\":\"https://api.github.com/users/elastic/repos\",\"events_url\":\"https://api.github.com/users/elastic/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/elastic/received_events\",\"type\":\"Organization\",\"site_admin\":false},\"private\":false,\"html_url\":\"https://github.com/elastic/elasticsearch\",\"description\":\"Open Source, Distributed, RESTful Search Engine\",\"fork\":false,\"url\":\"https://api.github.com/repos/elastic/elasticsearch\",\"forks_url\":\"https://api.github.com/repos/elastic/elasticsearch/forks\",\"keys_url\":\"https://api.github.com/repos/elastic/elasticsearch/keys{/key_id}\",\"collaborators_url\":\"https://api.github.com/repos/elastic/elasticsearch/collaborators{/collaborator}\",\"teams_url\":\"https://api.github.com/repos/elastic/elasticsearch/teams\",\"hooks_url\":\"https://api.github.com/repos/elastic/elasticsearch/hooks\",\"issue_events_url\":\"https://api.github.com/repos/elastic/elasticsearch/issues/events{/number}\",\"events_url\":\"https://api.github.com/repos/elastic/elasticsearch/events\",\"assignees_url\":\"https://api.github.com/repos/elastic/elasticsearch/assignees{/user}\",\"branches_url\":\"https://api.github.com/repos/elastic/elasticsearch/branches{/branch}\",\"tags_url\":\"https://api.github.com/repos/elastic/elasticsearch/tags\",\"blobs_url\":\"https://api.github.com/repos/elastic/elasticsearch/git/blobs{/sha}\",\"git_tags_url\":\"https://api.github.com/repos/elastic/elasticsearch/git/tags{/sha}\",\"git_refs_url\":\"https://api.github.com/repos/elastic/elasticsearch/git/refs{/sha}\",\"trees_url\":\"https://api.github.com/repos/elastic/elasticsearch/git/trees{/sha}\",\"statuses_url\":\"https://api.github.com/repos/elastic/elasticsearch/statuses/{sha}\",\"languages_url\":\"https://api.github.com/repos/elastic/elasticsearch/languages\",\"stargazers_url\":\"https://api.github.com/repos/elastic/elasticsearch/stargazers\",\"contributors_url\":\"https://api.github.com/repos/elastic/elasticsearch/contributors\",\"subscribers_url\":\"https://api.github.com/repos/elastic/elasticsearch/subscribers\",\"subscription_url\":\"https://api.github.com/repos/elastic/elasticsearch/subscription\",\"commits_url\":\"https://api.github.com/repos/elastic/elasticsearch/commits{/sha}\",\"git_commits_url\":\"https://api.github.com/repos/elastic/elasticsearch/git/commits{/sha}\",\"comments_url\":\"https://api.github.com/repos/elastic/elasticsearch/comments{/number}\",\"issue_comment_url\":\"https://api.github.com/repos/elastic/elasticsearch/issues/comments{/number}\",\"contents_url\":\"https://api.github.com/repos/elastic/elasticsearch/contents/{+path}\",\"compare_url\":\"https://api.github.com/repos/elastic/elasticsearch/compare/{base}...{head}\",\"merges_url\":\"https://api.github.com/repos/elastic/elasticsearch/merges\",\"archive_url\":\"https://api.github.com/repos/elastic/elasticsearch/{archive_format}{/ref}\",\"downloads_url\":\"https://api.github.com/repos/elastic/elasticsearch/downloads\",\"issues_url\":\"https://api.github.com/repos/elastic/elasticsearch/issues{/number}\",\"pulls_url\":\"https://api.github.com/repos/elastic/elasticsearch/pulls{/number}\",\"milestones_url\":\"https://api.github.com/repos/elastic/elasticsearch/milestones{/number}\",\"notifications_url\":\"https://api.github.com/repos/elastic/elasticsearch/notifications{?since,all,participating}\",\"labels_url\":\"https://api.github.com/repos/elastic/elasticsearch/labels{/name}\",\"releases_url\":\"https://api.github.com/repos/elastic/elasticsearch/releases{/id}\",\"deployments_url\":\"https://api.github.com/repos/elastic/elasticsearch/deployments\",\"created_at\":\"2010-02-08T13:20:56Z\",\"updated_at\":\"2016-12-06T18:03:57Z\",\"pushed_at\":\"2016-12-06T18:13:59Z\",\"git_url\":\"git://github.com/elastic/elasticsearch.git\",\"ssh_url\":\"git@github.com:elastic/elasticsearch.git\",\"clone_url\":\"https://github.com/elastic/elasticsearch.git\",\"svn_url\":\"https://github.com/elastic/elasticsearch\",\"homepage\":\"https://www.elastic.co/products/elasticsearch\",\"size\":381689,\"stargazers_count\":19665,\"watchers_count\":19665,\"language\":\"Java\",\"has_issues\":true,\"has_downloads\":true,\"has_wiki\":false,\"has_pages\":false,\"forks_count\":6757,\"mirror_url\":null,\"open_issues_count\":1186,\"forks\":6757,\"open_issues\":1186,\"watchers\":19665,\"default_branch\":\"master\",\"score\":1}]}".data(using: String.Encoding.utf8)!
        }
    }

    var task: Task {
        return .request
    }

    var url: String {
        return "\(baseURL)\(path)".urlEscaped
    }

}

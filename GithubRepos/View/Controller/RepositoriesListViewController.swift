//
//  RepositoriesListViewController.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import NSObject_Rx
import StatefulViewController

class RepositoriesListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    fileprivate let dataSource = RxTableViewSectionedReloadDataSource<SectionViewModel<RepositoryItemViewModel>>()

    var viewModel: RepositoriesListViewModel!

}

// MARK: Life Cycle

extension RepositoriesListViewController: LoadingStatePresentableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        setupTableViewDataSource()
        setupNavigationBar()
        setupLoadingState()

        viewModel.fetchRepositories()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupInitialViewState()

        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

}

// MARK: StatefulViewController

extension RepositoriesListViewController: StatefulViewController {

    func hasContent() -> Bool {
        return viewModel.hasContent
    }

}

// MARK: Setup

extension RepositoriesListViewController {

    fileprivate func setupNavigationBar() {
        title = "Github JavaPop"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    fileprivate func setupTableView() {
        tableView.register(R.nib.repositoryCell)
        tableView.rowHeight = 113
    }

    fileprivate func setupTableViewDataSource() {
        dataSource.configureCell = { _, tableView, indexPath, viewModel in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.repositoryCell) else {
                fatalError()
            }

            return cell
        }

        tableView.rx.willDisplayCell
            .subscribe(onNext: { (cell: UITableViewCell, indexPath: IndexPath) in
                guard let repoCell = cell as? RepositoryCell else { return }
                guard let viewModel = try? self.dataSource.model(at: indexPath) as? RepositoryItemViewModel else { return }

                repoCell.viewModel = viewModel!
            }).addDisposableTo(rx_disposeBag)

        tableView.rx.modelSelected(RepositoryItemViewModel.self)
            .subscribe(onNext: { (item) in
                self.viewModel.showRepoDetail(item: item.createDetailViewModel())
            }).addDisposableTo(rx_disposeBag)

        let offset = view.bounds.height * 0.6
        viewModel.nextPageTrigger = tableView.rx.contentOffset
            .flatMap { _ in
                self.tableView.isNearBottomEdge(edgeOffset: offset) ? Observable.just(()) : Observable.empty()
            }

        viewModel.repositories
            .drive(tableView.rx.items(dataSource: dataSource))
            .addDisposableTo(rx_disposeBag)

        viewModel.loadingState
            .bindTo(rx.isLoading)
            .addDisposableTo(rx_disposeBag)
    }

}

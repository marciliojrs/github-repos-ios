//
//  RepositoryDetailViewController.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 08/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import NSObject_Rx
import StatefulViewController

class RepositoryDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    fileprivate let dataSource = RxTableViewSectionedReloadDataSource<SectionViewModel<PullRequestItemViewModel>>()

    var viewModel: RepositoryDetailViewModel!

}

// MARK: Life Cycle

extension RepositoryDetailViewController: LoadingStatePresentableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        setupTableView()
        setupTableViewDataSource()
        setupLoadingState()

        viewModel.fetchPullRequests()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupInitialViewState()

        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

}

// MARK: StatefulViewController

extension RepositoryDetailViewController: StatefulViewController {

    func hasContent() -> Bool {
        return viewModel.hasContent
    }

}

// MARK: Setup UI

extension RepositoryDetailViewController {

    fileprivate func setupNavigationBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        viewModel.repositoryName
            .drive(rx.title)
            .addDisposableTo(rx_disposeBag)
    }

    fileprivate func setupTableView() {
        tableView.register(R.nib.pullRequestCell)
        tableView.rowHeight = 112
    }

    fileprivate func setupTableViewDataSource() {
        dataSource.configureCell = { _, tableView, indexPath, viewModel in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.pullRequestCell) else {
                fatalError()
            }

            return cell
        }

        tableView.rx.willDisplayCell
            .subscribe(onNext: { (cell: UITableViewCell, indexPath: IndexPath) in
                guard let cell = cell as? PullRequestCell else { return }
                guard let viewModel = try? self.dataSource.model(at: indexPath) as? PullRequestItemViewModel else { return }

                cell.viewModel = viewModel!
            }).addDisposableTo(rx_disposeBag)

        tableView.rx.modelSelected(PullRequestItemViewModel.self)
            .subscribe(onNext: { (item) in
                self.viewModel.showPullRequest(item: item.createBrowserViewModel())
            }).addDisposableTo(rx_disposeBag)

        viewModel.pullRequests
            .drive(tableView.rx.items(dataSource: dataSource))
            .addDisposableTo(rx_disposeBag)

        viewModel.loadingState
            .bindTo(rx.isLoading)
            .addDisposableTo(rx_disposeBag)
    }

}

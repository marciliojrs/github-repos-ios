//
//  BrowserViewController.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 08/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

class BrowserViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!

    var viewModel: BrowserViewModel!

}

// MARK: Life Cycle

extension BrowserViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.numberPR
            .drive(rx.title)
            .addDisposableTo(rx_disposeBag)

        webView.loadRequest(viewModel.urlRequest)
    }

}

//
//  PullRequestCell.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 08/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

class PullRequestCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!

    var viewModel: PullRequestItemViewModel! {
        didSet {
            viewModel.title
                .drive(titleLabel.rx.text)
                .addDisposableTo(rx_disposeBag)

            viewModel.avatarImage
                .drive(avatarImageView.rx.downloadableImage)
                .addDisposableTo(rx_disposeBag)

            viewModel.body
                .drive(bodyLabel.rx.text)
                .addDisposableTo(rx_disposeBag)

            viewModel.date
                .drive(dateLabel.rx.text)
                .addDisposableTo(rx_disposeBag)

            viewModel.userLogin
                .drive(usernameLabel.rx.text)
                .addDisposableTo(rx_disposeBag)
        }
    }

}

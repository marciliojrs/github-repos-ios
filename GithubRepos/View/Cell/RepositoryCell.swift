//
//  RepositoryCell.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit
import NSObject_Rx

class RepositoryCell: UITableViewCell {

    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var repoDescriptionLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!
    @IBOutlet weak var repoFullNameLabel: UILabel!
    @IBOutlet weak var ownerImageView: UIImageView!

    var viewModel: RepositoryItemViewModel! {
        didSet {
            viewModel.repositoryName
                .drive(repoNameLabel.rx.text)
                .addDisposableTo(rx_disposeBag)

            viewModel.numberOfStars
                .drive(starsLabel.rx.text)
                .addDisposableTo(rx_disposeBag)

            viewModel.numberOfForks
                .drive(forksLabel.rx.text)
                .addDisposableTo(rx_disposeBag)

            viewModel.avatarImage
                .drive(ownerImageView.rx.downloadableImage)
                .addDisposableTo(rx_disposeBag)

            viewModel.ownerLogin
                .drive(ownerLabel.rx.text)
                .addDisposableTo(rx_disposeBag)

            viewModel.repoFullname
                .drive(repoFullNameLabel.rx.text)
                .addDisposableTo(rx_disposeBag)

            viewModel.repoDescription
                .drive(repoDescriptionLabel.rx.text)
                .addDisposableTo(rx_disposeBag)
        }
    }

}

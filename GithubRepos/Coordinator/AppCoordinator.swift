//
//  AppCoordinator.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 06/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {

    enum CoordinatorKey: String {
        case repositories
    }

    var window: UIWindow
    var childCoordinators: [String: Coordinator] = [:]

    required init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let reposCoordinator = RepositoriesCoordinator(window: window)
        childCoordinators[CoordinatorKey.repositories.rawValue] = reposCoordinator
        reposCoordinator.start()
    }

}

//
//  Coordinator.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 06/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

protocol Coordinator: class {

    init(window: UIWindow)

    func start()

}

//
//  SectionViewModel.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import RxDataSources

final class SectionViewModel<T>: ViewModel {

    let title: String?
    let viewModels: [T]

    init(title: String? = nil, viewModels: [T]) {
        self.title = title
        self.viewModels = viewModels
    }

}

extension SectionViewModel: SectionModelType {

    typealias Item = T

    var items: [Item] {
        return viewModels
    }

    convenience init(original: SectionViewModel, items: [Item]) {
        self.init(title: original.title, viewModels: items)
    }

}

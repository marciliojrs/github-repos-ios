//
//  DownloadableImage.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

enum DownloadableImage {
    case remote(url: URL?, placeholder: UIImage?)
    case local(image: UIImage?)
}

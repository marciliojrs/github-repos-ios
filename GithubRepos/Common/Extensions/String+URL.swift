//
//  String+URL.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Foundation

extension String {

    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

}

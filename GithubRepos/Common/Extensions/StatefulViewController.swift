//
//  StatefulViewController.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 09/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import RxSwift
import RxCocoa
import StatefulViewController

extension Reactive where Base: StatefulViewController {

    var isLoading: UIBindingObserver<Base, Bool> {
        return UIBindingObserver(UIElement: self.base) { control, value in
            control.startLoading()
        }
    }

}

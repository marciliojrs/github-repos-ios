//
//  AppDelegate.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 12/6/16
//  Copyright (c) 2016 Concrete Solutions. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        window = UIWindow()
        appCoordinator = AppCoordinator(window: window!)
        appCoordinator.start()

        window?.makeKeyAndVisible()

        return true
    }

}

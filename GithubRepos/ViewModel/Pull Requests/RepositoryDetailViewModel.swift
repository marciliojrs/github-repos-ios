//
//  RepositoryDetailViewModel.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 08/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import RxSwift
import RxCocoa

class RepositoryDetailViewModel {

    fileprivate var apiClient = NetworkClient()
    fileprivate let disposeBag = DisposeBag()
    fileprivate let pullsVariable: Variable<[PullRequest]>
    fileprivate var loadingStateVariable: Variable<Bool>

    var coordinator: RepositoriesCoordinator!
    let repository: Repository
    var loadingState: Observable<Bool> { return loadingStateVariable.asObservable() }
    var hasContent: Bool {
        return loadingStateVariable.value ? false : pullsVariable.value.count > 0
    }

    init(repository: Repository) {
        self.repository = repository
        self.pullsVariable = Variable([])
        self.loadingStateVariable = Variable(false)
    }

}

// MARK: Computed Property

extension RepositoryDetailViewModel {

    var repositoryName: Driver<String> {
        return Driver.just(repository.name)
    }

    var pullRequests: Driver<[SectionViewModel<PullRequestItemViewModel>]> {
        return pullsViewModel
            .asDriver(onErrorJustReturn: [])
            .map {
                [SectionViewModel(viewModels: $0)]
            }
    }

    var pullsViewModel: Observable<[PullRequestItemViewModel]> {
        return pullsVariable.asObservable()
            .map { (pulls) in
                return pulls.map { repo in
                    return PullRequestItemViewModel(model: repo)
                }
            }
    }

}

extension RepositoryDetailViewModel {

    func fetchPullRequests() {
        loadingStateVariable.value = true
        apiClient.loadPullRequest(forRepo: repository.name, owner: repository.owner.login)
            .subscribe(onNext: { (pulls) in
                self.pullsVariable.value = pulls
                self.loadingStateVariable.value = false
            }).addDisposableTo(disposeBag)
    }

    func showPullRequest(item: BrowserViewModel) {
        coordinator.showPullRequest(viewModel: item)
    }

}

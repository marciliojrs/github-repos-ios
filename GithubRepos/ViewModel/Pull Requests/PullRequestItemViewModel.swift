//
//  PullRequestItemViewModel.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 08/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import RxCocoa
import SwiftDate

class PullRequestItemViewModel: ViewModel {

    let model: PullRequest

    init(model: PullRequest) {
        self.model = model
    }

}

extension PullRequestItemViewModel {

    var title: Driver<String> {
        return Driver.just(model.title)
    }

    var avatarImage: Driver<DownloadableImage> {
        return Driver.just(.remote(url: model.user.avatarUrl, placeholder: R.image.github()))
    }

    var userLogin: Driver<String> {
        return Driver.just(model.user.login)
    }

    var body: Driver<String> {
        return Driver.just(model.body)
    }

    var date: Driver<String> {
        return Driver.just(model.createdAt.string(format: .custom("dd/MM/yyyy")))
    }

}

extension PullRequestItemViewModel {

    func createBrowserViewModel() -> BrowserViewModel {
        return BrowserViewModel(model: model)
    }

}

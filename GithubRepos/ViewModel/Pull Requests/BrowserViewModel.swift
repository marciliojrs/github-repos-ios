//
//  BrowserViewModel.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 08/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import RxCocoa

class BrowserViewModel: ViewModel {

    let model: PullRequest

    init(model: PullRequest) {
        self.model = model
    }

    var urlRequest: URLRequest {
        return URLRequest(url: model.url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10)
    }

    var numberPR: Driver<String> {
        return Driver.just(String(model.number))
    }

}

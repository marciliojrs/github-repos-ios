//
//  RepositoriesListViewModel.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class RepositoriesListViewModel: ViewModel {

    fileprivate var repositoriesVariable: Variable<[Repository]>
    fileprivate var loadingStateVariable: Variable<Bool>
    fileprivate var apiClient = NetworkClient()
    fileprivate let disposeBag = DisposeBag()

    var coordinator: RepositoriesCoordinator!
    var nextPageTrigger: Observable<Void> = .empty()
    var loadingState: Observable<Bool> { return loadingStateVariable.asObservable() }
    var hasContent: Bool {
        return loadingStateVariable.value ? false : repositoriesVariable.value.count > 0
    }

    init() {
        repositoriesVariable = Variable([])
        loadingStateVariable = Variable(false)
    }

}

// MARK: Computed Property

extension RepositoriesListViewModel {

    var repositories: Driver<[SectionViewModel<RepositoryItemViewModel>]> {
        return repositoriesViewModel
            .asDriver(onErrorJustReturn: [])
            .map {
                [SectionViewModel(viewModels: $0)]
            }
    }

    var repositoriesViewModel: Observable<[RepositoryItemViewModel]> {
        return repositoriesVariable.asObservable()
            .map { (repositories) in
                return repositories.map { repo in
                    return RepositoryItemViewModel(model: repo)
                }
            }
    }

}

// MARK: Helpers

extension RepositoriesListViewModel {

    func fetchRepositories() {
        loadingStateVariable.value = true
        apiClient.searchRepositories(nextPageTrigger: nextPageTrigger)
            .subscribe(onNext: { (repositories) in
                self.repositoriesVariable.value = repositories
                self.loadingStateVariable.value = false
            }).addDisposableTo(disposeBag)
    }

    func showRepoDetail(item: RepositoryDetailViewModel) {
        coordinator.showRepoDetail(viewModel: item)
    }

}

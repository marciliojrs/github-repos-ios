//
//  RepositoryItemCellViewModel.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class RepositoryItemViewModel: ViewModel {

    let model: Repository

    init(model: Repository) {
        self.model = model
    }

}

extension RepositoryItemViewModel {

    var repositoryName: Driver<String> {
        return Driver.just(model.name)
    }

    var numberOfForks: Driver<String> {
        return Driver.just(String(model.numberOfForks))
    }

    var numberOfStars: Driver<String> {
        return Driver.just(String(model.numberOfStars))
    }

    var avatarImage: Driver<DownloadableImage> {
        return Driver.just(.remote(url: model.owner.avatarUrl, placeholder: R.image.github()))
    }

    var ownerLogin: Driver<String> {
        return Driver.just(model.owner.login)
    }

    var repoFullname: Driver<String> {
        return Driver.just(model.fullname)
    }

    var repoDescription: Driver<String> {
        return Driver.just(model.description ?? "")
    }

}

extension RepositoryItemViewModel {

    func createDetailViewModel() -> RepositoryDetailViewModel {
        return RepositoryDetailViewModel(repository: model)
    }

}

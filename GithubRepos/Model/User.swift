//
//  Owner.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import ObjectMapper

struct User: Mappable {

    enum Key: String {
        case id, login, avatarUrl = "avatar_url"
    }

    // swiftlint:disable variable_name
    var id: Int!
    var login: String!
    var avatarUrl: URL!

    init?(map: Map) { }

    mutating func mapping(map: Map) {
        id          <- map[Key.id.rawValue]
        login       <- map[Key.login.rawValue]
        avatarUrl   <- (map[Key.avatarUrl.rawValue], URLTransform())
    }

}

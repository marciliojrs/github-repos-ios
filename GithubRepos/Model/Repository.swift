//
//  Repository.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 06/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import ObjectMapper

struct Repository: Mappable {

    enum Key: String {
        case id, name, numberOfForks = "forks_count", numberOfStars = "stargazers_count", owner, fullname = "full_name", description
    }

    // swiftlint:disable variable_name
    var id: Int!
    var name: String!
    var numberOfForks: Int!
    var numberOfStars: Int!
    var fullname: String!
    var description: String?
    var owner: User!

    init?(map: Map) { }

    mutating func mapping(map: Map) {
        id              <- map[Key.id.rawValue]
        name            <- map[Key.name.rawValue]
        numberOfForks   <- map[Key.numberOfForks.rawValue]
        numberOfStars   <- map[Key.numberOfStars.rawValue]
        fullname        <- map[Key.fullname.rawValue]
        description     <- map[Key.description.rawValue]
        owner           <- map[Key.owner.rawValue]
    }

}

//
//  GithubRestApiSpec.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 06/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Quick
import Nimble
import Moya
import NSObject_Rx
import RxSwift
@testable import GithubRepos

class GithubRestApiSpec: QuickSpec {

    override func spec() {
        var testProvider: RxMoyaProvider<MultiTarget>!
        var networkClient: NetworkClient!

        beforeEach {
            testProvider = RxMoyaProvider(stubClosure: MoyaProvider.immediatelyStub)
            networkClient = NetworkClient(provider: testProvider)
        }

        describe("NetworkClient") {
            context("when subscribe search repositories observable") {
                it("should emit a custom response with one repository") {
                    waitUntil { done in
                        networkClient.searchRepositories(nextPageTrigger: Observable.empty())
                            .observeOn(MainScheduler.instance)
                            .skip(1) // ignore start element
                            .subscribe(onNext: { (repositories) in
                                expect(repositories.count) == 1

                                guard let repository = repositories.first else {
                                    fail("nil repository")
                                    return
                                }

                                expect(repository.id) == 507775
                                done()
                            }).addDisposableTo(self.rx_disposeBag)
                    }
                }
            }
        }
    }

}

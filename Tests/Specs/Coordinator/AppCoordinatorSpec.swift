//
//  AppCoordinator.swift
//  GithubRepos
//
//  Created by Marcilio Junior on 07/12/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Quick
import Nimble
@testable import GithubRepos

class AppCoordinatorSpec: QuickSpec {

    override func spec() {
        var appCoordinator: AppCoordinator!
        let mockedWindow = UIWindow()

        beforeEach {
            appCoordinator = AppCoordinator(window: mockedWindow)
        }

        describe("AppCoordinator") {
            context("when starts") {
                beforeEach {
                    appCoordinator.start()
                }

                it("should start RepositoriesCoordinator") {
                    let childCoordinator: Coordinator? = appCoordinator.childCoordinators[AppCoordinator.CoordinatorKey.repositories.rawValue]
                    expect(childCoordinator).toNot(beNil())
                    expect(childCoordinator is RepositoriesCoordinator) == true

                    let navigationController = appCoordinator.window.rootViewController
                    expect(navigationController is UINavigationController) == true
                    expect((navigationController as? UINavigationController)?.topViewController is RepositoriesListViewController) == true
                    expect(mockedWindow.rootViewController) == navigationController
                }
            }
        }
    }

}
